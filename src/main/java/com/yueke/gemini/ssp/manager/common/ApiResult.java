package com.yueke.gemini.ssp.manager.common;

import com.alibaba.fastjson.JSON;

/**
 * Created by admin on 2019/3/5.
 */
public final class ApiResult<T> {

    int resultCode;
    String resultMsg;
    T resultObject;


    public ApiResult() {
        this.resultCode = 200;
        this.resultMsg = "ok";
    }


    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    public T getResultObject() {
        return resultObject;
    }

    public void setResultObject(T resultObject) {
        this.resultObject = resultObject;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
