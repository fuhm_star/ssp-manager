package com.yueke.gemini.ssp.manager.handler;

import com.yueke.gemini.ssp.manager.common.ApiResult;
import com.yueke.gemini.ssp.manager.exception.ApiException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 全局异常处理
 *
 * Created by admin on 2019/3/5.
 */
@ControllerAdvice
@ResponseBody
public class SspExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public ApiResult<String> exceptionHandler(HttpServletRequest request, Exception e){
        //全局异常处理逻辑
        if(e instanceof ApiException){
            return new ApiResult<>();
        }
        //绑定异常处理逻辑
        //TODO

        return null;
    }

}
