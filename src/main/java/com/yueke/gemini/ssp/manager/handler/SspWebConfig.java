package com.yueke.gemini.ssp.manager.handler;

import com.yueke.gemini.ssp.manager.interceptor.ApiInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 配置类
 * Created by admin on 2019/3/5.
 */
@Configuration
public class SspWebConfig implements WebMvcConfigurer {

    @Autowired
    private ApiInterceptor apiInterceptor;

    /**
     * 添加拦截器
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(apiInterceptor).addPathPatterns("/");
    }
}
