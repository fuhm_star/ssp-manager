package com.yueke.gemini.ssp.manager.entity;

import java.io.Serializable;

/**
 * Created by admin on 2019/3/5.
 */
public class User implements Serializable {

    private String username;
    private String password;

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
