package com.yueke.gemini.ssp.manager.controller;

import com.yueke.gemini.ssp.manager.common.ApiResult;
import com.yueke.gemini.ssp.manager.entity.User;
import com.yueke.gemini.ssp.manager.entity.UserInfo;
import com.yueke.gemini.ssp.manager.exception.ApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by admin on 2019/3/5.
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    /**
     * 登陆
     *
     * @param user
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    @ResponseBody
    public ApiResult<UserInfo> login(@RequestBody User user) throws ApiException {
        /*
         * init
         */
        Boolean action = true;
        final ApiResult<UserInfo> apiResult = new ApiResult<>();

        /*
         * validation
         */
        action = action && user != null;

        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("admin");

//        if (true){
//            throw new ApiException("错误");
//        }


        apiResult.setResultCode(200);
        apiResult.setResultMsg("登陆成功！");
        apiResult.setResultObject(userInfo);

        return apiResult;
    }
}
