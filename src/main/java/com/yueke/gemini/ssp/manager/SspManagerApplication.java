package com.yueke.gemini.ssp.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SspManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SspManagerApplication.class, args);
	}

}
